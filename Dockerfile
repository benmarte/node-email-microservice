FROM node:12.18-alpine
ENV NODE_ENV=production
WORKDIR /opt/node-email-microservice
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN yarn --production --silent && mv node_modules ../
COPY . .
EXPOSE 3030
CMD ["yarn", "start"]
