require("dotenv").config();

const MailService = require("./MailService");

const express = require("express");

const cors = require("cors");

const mailService = new MailService();

const app = express();

const allowlist = process.env.ALLOW_LIST; // list of domains that can access the API via CORS

const port = 3030;

const corsOptionsDelegate = (req, callback) => {

  let corsOptions;

  let isDomainAllowed = allowlist.indexOf(req.header('Origin')) !== -1;

  if (isDomainAllowed) {
    // Enable CORS for this request
    corsOptions = { origin: true }
  } else {
    // Disable CORS for this request
    corsOptions = { origin: false }
  }
  callback(null, corsOptions)
}

app.use(cors(corsOptionsDelegate)); // middleware that can be used to enable CORS with various options.

app.use(express.urlencoded({ extended: true })); // parses incoming requests with urlencoded payloads

app.use(express.json()); // parses incoming requests with JSON payloads 

app.get('/', function (req, res) {

  res.send('Welcome to Micro Email Service'); // welcome message

});

app.post("/", async (req, res) => {

  try {

    const { message, title, to } = req.body; // destructure params from body here

    const details = { // create the details object with the values we will pass to our handlebars template as context
      title,
      message,
      to
    };

    const mailInfo = {
      to, // email recipient
      subject: `Email subject: ${title}`, // email subject with injected title 
      template: "sample", // the name of the email template to use located in the views folder
      context: details // the object with the values we are going to inject to our handlebars template
    };

    await mailService.sendMail(mailInfo); // send the email

    res.send("Your e-mail has been sent successfully!"); // success message

  } catch (e) {

    res.status(500).send("Something broke!"); // error message

  }

});

app.listen(port, () => console.log(`app listening on port ${port}!`));