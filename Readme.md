# Mailgun Email Microservice with Heroku CI/CD pipeline

An express API microservice to send emails using mailgun with auto deploy on push to Heroku.

## Prerequisites

- Mailgun account
- Heroku account
- Docker
- DOcker Compose
- VScode

## Running locally with docker run

- Run `docker build -t node-email-microservice .` to build the docker image
- Run `docker run --env-file=.env -p 3030:3030 node-email-microservice` to run the app

## Running locally with docker-compose

Edit `docker-compose.yml` environment variable values:

```
SMTP_HOST: "smtp.domain.com"
SMTP_PORT: 587
SMTP_USER: "mailgun smtp user"
SMTP_PASSWORD: "mailgun or other service smtp password"
FROM_EMAIL: "no-reply@your.domain.com"
DOMAIN: "your.domain.com"
MAILGUN_API_KEY: "your mailgun api key"
ALLOW_LIST: "single or multiple domains to allow, values must be a single string separated by commas ie: http://localhost, http://localhost:3000"
```

- Run `docker-compose up` to start the application

## Running locally with npm or yarn

Create a .env file with the following environment variables and their respective values.

```
SMTP_HOST: "smtp.domain.com"
SMTP_PORT: 587
SMTP_USER: "mailgun smtp user"
SMTP_PASSWORD: "mailgun or other service smtp password"
FROM_EMAIL: "no-reply@your.domain.com"
DOMAIN: "your.domain.com"
MAILGUN_API_KEY: "your mailgun api key"
ALLOW_LIST: "single or multiple domains to allow, values must be a single string separated by commas ie: http://localhost, http://localhost:3000"
```

- install dependencies via: `yarn` or `npm i` 
- Then run: `yarn start` or `npm start`

## How to debug in vscode with docker-compose

- Click the debug icon in VScode
- Ensure you have `Docker: Debug Node` selected in the drop down and click play
- VScode will build and open your browser with the app in debug mode
- Place a breakpoint anywhere in your code and refresh the app in your browser

![How to debug in vscode with docker-compose](/uploads/66bd6d83523aa56477420d883d52e6a3/Screen+Recording+2021-04-12+at+09.26.25.17+AM.gif)

## Debugging not working

If when you start a debugging session and it does not launch your app in the browser then you will have to do the following:

- Open a new terminal window
- Run `docker ps -a` identify your `nodeemailmicroservice` container port
- Navigate to `http://localhost:${container_port}` in this example my debugging port is: `49242` so I would need to enter: `http://localhost:49242` in my browser in order to start debugging.

![Debugging not working](/uploads/e8eca808263e63e6b25d37002b6f47c8/Screenshot_2021-04-13_114651.png)

## Gitlab Environment Variables

In order to have your app auto deploy on push to Heroku you need to create a few environment variables in Gitlab, these are:

- CI_REGISTRY - your gitlab registry url (usually prepend registry to your gitlab repo url)
- CI_REGISTRY_IMAGE - the name you want to use for your docker container
- CI_REGISTRY_PASSWORD - Setup a [personal access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)
- CI_REGISTRY_USER - your gitlab username
- HEROKU_API_KEY - In Heroku go to your account settings and copy your API key or generate a new one
- HEROKU_APP - The name of your Heroku app

Ensure all of your Gitlab environment variables are **protected** and **masked** when possible to ensure the security of these.

![Gitlab ENV Vars](/uploads/3788f9d08f3e7c5902e91be02107a20a/image.png)

Now everytime you push changes to your repo Gitlab CI/CD will auto deploy to your heroku app.

## Heroku environment variables

When developing locally we use a `.env` file which has all the environment variable our app needs in order to run. Since we should never check in `.env` files in your repo we need to ensure these same environment variables exist on heroku in order for our app to work.

In Heroku go to your project settings and create environment variables for the following:

```
SMTP_HOST: "smtp.domain.com"
SMTP_PORT: 587
SMTP_USER: "mailgun smtp user"
SMTP_PASSWORD: "mailgun or other service smtp password"
FROM_EMAIL: "no-reply@your.domain.com"
DOMAIN: "your.domain.com"
MAILGUN_API_KEY: "your mailgun api key"
ALLOW_LIST: "single or multiple domains to allow, values must be a single string separated by commas ie: http://localhost, http://localhost:3000"
```

![image](/uploads/41d1c37e27168c489a75ddb5be5f01f3/image.png)

Once you have created these environement variables your app should be working in Heroku on your next push.

## Email templates

This project uses [handlebarsjs](https://handlebarsjs.com/) for email templating.

Email layouts, partials and templates are located in the views folder. The layouts folder contains the `main.hbs` which is our base html template with a code block `{{{body}}}` which is where our email templates will be rendered. The partials folder is empty and the views folder contains a `sample.hbs` template which has the **details** object values we pass to our mailInfo object as context on [line 57 of index.js ](https://gitlab.com/benmarte/node-email-microservice/-/blob/master/index.js#L57).

You can have multiple endpoints with varying email templates for each endpoint, just copy/paste the existing `app.post` endpoint (lines [45](https://gitlab.com/benmarte/node-email-microservice/-/blob/master/index.js#L45)-[74](https://gitlab.com/benmarte/node-email-microservice/-/blob/master/index.js#L74)) ensure you make a new route and edit both the details and mailInfo objects accordingly.

Happy emailing.
